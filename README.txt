About this Application:

This is a Static Web application which will connect different Ec2 resources.

Once the application is deployed on Tomcat server. The application index page will be displayed with Links to different Instances.

On clicking on the links the page will be redirecting us to the Specific instance and appropriate html pages will be displayed.

Check ConfigEc2.txt file to get understanding about the EC2 instance in folder EC2 instance under Code directory.

To Access static web Application: http://367437-env.us-east-2.elasticbeanstalk.com/ this is the Elastic Beanstalk url.